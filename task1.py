# Вы приехали помогать на ферму Дядюшки Джо и видите вокруг себя множество разных животных:

# гусей "Серый" и "Белый"
# корову "Маньку"
# овец "Барашек" и "Кудрявый"
# кур "Ко-Ко" и "Кукареку"
# коз "Рога" и "Копыта"
# и утку "Кряква"
# Со всеми животными вам необходимо как-то взаимодействовать:

# кормить
# корову и коз доить
# овец стричь
# собирать яйца у кур, утки и гусей
# различать по голосам(коровы мычат, утки крякают и т.д.)
# Задача №1
# Нужно реализовать классы животных, не забывая использовать наследование, определить общие методы взаимодействия с животными и дополнить их в дочерних классах, если потребуется.


class Animal:
  voice = None
  def __init__(self, name, weight):
       self.name = name
       self.weight = weight

  def feed(self):  # покормить животное
       print('Животное по имени', self.name, 'накормлено')
  def voices(self):  # узнать какой голос у животного
       print('Животное по имени', self.name, self.voice)

class Bird(Animal):
  def __init__(self, name, weight, eggs):
    super().__init__(name, weight)
    self.eggs = eggs
  def collect_eggs(self):  # собрать снесенные яйца
    print('У птицы по имени', self.name, 'собрано', self.eggs, 'яиц(а)')


class Goose(Bird):
  voice = 'гогочет'


class Duck(Bird):
  voice = 'шипит'


class Chicken(Bird):
  voice = 'кудахчет'


class Cow(Animal):
  voice = 'мычит'
  def __init__(self, name, weight, milk):
    super().__init__(name, weight=weight)
    self.milk = milk
  def get_milk(self):
    print('От коровы по имени', self.name, 'получено', self.milk, 'литра(ов) молока')

class Goat(Animal):
  voice = 'блеет'
  def __init__(self, name, weight, milk):
    super().__init__(name, weight=weight)
    self.milk = milk
  def get_milk(self):
    print('От козы по имени', self.name, 'получено', self.milk, 'литра(ов) молока')


class Sheep(Animal):
  voice = 'блеет'
  def trim(self):
    print('Овца по имени', self.name, 'пострижена')


# Задача №2
# Для каждого животного из списка должен существовать экземпляр класса.
# Каждое животное требуется накормить и подоить/постричь/собрать яйца, если надо.

goose_grey = Goose('Серый', 2, '5')
goose_grey.feed()
goose_grey.voices()
goose_grey.collect_eggs()

goose_white = Goose('Белый', 3, '3')
goose_white.feed()
goose_white.voices()
goose_white.collect_eggs()

chicken_koko = Chicken('Ко-ко', 3, '9')
chicken_koko.feed()
chicken_koko.voices()
chicken_koko.collect_eggs()

chicken_kukareku = Chicken('Кукареку', 4, '12')
chicken_kukareku.feed()
chicken_kukareku.voices()
chicken_kukareku.collect_eggs()

duck_kriakva = Duck('Кряква', 2, '7')
duck_kriakva.feed()
duck_kriakva.voices()
duck_kriakva.collect_eggs()

cow_manechka = Cow('Манька', 90, '9')
cow_manechka.feed()
cow_manechka.voices()
cow_manechka.get_milk()

goat_horns = Goat('Рога', 30, '3')
goat_horns.feed()
goat_horns.voices()
goat_horns.get_milk()

goat_hooves = Goat('Копыта', 27, '2')
goat_hooves.feed()
goat_hooves.voices()
goat_hooves.get_milk()

sheep_barashek = Sheep('Барашек', 18)
sheep_barashek.feed()
sheep_barashek.voices()
sheep_barashek.trim()

sheep_curly = Sheep('Кудрявый', 15)
sheep_curly.feed()
sheep_curly.voices()
sheep_curly.trim()



# Задача № 3
# У каждого животного должно быть определено имя(self.name) и вес(self.weight).
# Необходимо посчитать общий вес всех животных(экземпляров класса);
# Вывести название самого тяжелого животного.

all_animals = (goose_grey, goose_white, chicken_koko, chicken_kukareku, duck_kriakva, cow_manechka, goat_hooves, goat_horns, sheep_curly, sheep_barashek)

total_weight = 0
for animal in all_animals:
  total_weight = total_weight + animal.weight 

highest_weight_animal = all_animals[0]
for animal in all_animals:
  if animal.weight > highest_weight_animal.weight:
    highest_weight_animal = animal
print('Самое тяжелое животное на ферме зовут', highest_weight_animal.name, 'оно весит', highest_weight_animal.weight, 'кг')


